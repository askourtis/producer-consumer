#ifndef QUEUE_H_GUARD
#define QUEUE_H_GUARD

#define QUEUE_SIZE 10

#include <pthread.h>
#include "WorkFunction.h"

typedef struct {
  WorkFunction buf[QUEUE_SIZE];
  long head, tail;
  int  full, empty;
  pthread_mutex_t *mut;
  pthread_cond_t  *notFull, *notEmpty;
} Queue;

Queue *queueInit(void);
void queueDelete(Queue *q);
void queueAdd(Queue *q, WorkFunction  in);
void queueDel(Queue *q, WorkFunction *out);

#endif