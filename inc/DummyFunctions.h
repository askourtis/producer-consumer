#ifndef DUMMYFUNCTIONS_H_GUARD
#define DUMMYFUNCTIONS_H_GUARD

extern void* (*dummyFunctions[4])(void*);

void *genericCos(void *);
void *genericSin(void *);
void *genericTimed(void *);
void *genericPrint(void *);


void dummy_init();

#endif