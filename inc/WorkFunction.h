#ifndef WORKFUNCTION_H_GUARD
#define WORKFUNCTION_H_GUARD


typedef struct {
  void *(*work)(void*);
  void *args;
} WorkFunction;


void execute(WorkFunction func);

#endif