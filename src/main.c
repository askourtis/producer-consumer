#include "Queue.h"
#include "WorkFunction.h"
#include "DummyFunctions.h"

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>


#define LOOP 1000000
#define P 20
#define Q 40

void *producer(void*);
void *consumer(void*);

int main ()
{
  Queue *fifo;
  pthread_t pro[P], con[Q];

  fifo = queueInit(); //TODO: Null check
  if (fifo ==  NULL) {
    fprintf (stderr, "main: Queue Init failed.\n");
    exit (1);
  }

  for(int i=0; i<Q; ++i)
    pthread_create (&con[i], NULL, consumer, fifo);
  for(int i=0; i<P; ++i)
    pthread_create (&pro[i], NULL, producer, fifo); //TODO: thread check
  for(int i=0; i<P; ++i)
    pthread_join (pro[i], NULL);
  for(int i=0; i<Q; ++i)
    pthread_join (con[i], NULL);
  queueDelete (fifo);

  return 0;
}


void *producer(void* args){
  Queue *fifo;
  int i;

  fifo = (Queue*)args;

  for (i = 0; i < LOOP; i++) {
    pthread_mutex_lock(fifo->mut);
    while (fifo->full) {
      pthread_cond_wait(fifo->notFull, fifo->mut);
    }
    struct timeval t_begin;
    gettimeofday(&t_begin, NULL);
    double *arg = malloc(sizeof(double));
    *arg = t_begin.tv_sec*1000000.0 + t_begin.tv_usec;
    queueAdd(fifo, (WorkFunction){dummyFunctions[rand()%4], (void*)arg});
    pthread_mutex_unlock(fifo->mut);
    pthread_cond_signal(fifo->notEmpty);
  }
  return (NULL);
}

void *consumer(void* args) {
  Queue *fifo;
  int i;
  WorkFunction d;

  fifo = (Queue*)args;

  while (1) {
    pthread_mutex_lock(fifo->mut);
    while (fifo->empty) {
      pthread_cond_wait(fifo->notEmpty, fifo->mut);
    }
    queueDel(fifo, &d);
    pthread_mutex_unlock(fifo->mut);
    pthread_cond_signal(fifo->notFull);
    execute(d);
  }

  return (NULL);
}