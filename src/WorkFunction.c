#include "WorkFunction.h"
#include <stdio.h>
#include <stdlib.h>

void execute(WorkFunction func) {
    free(func.work(func.args));
    //TODO: Handle return
    free(func.args);
}