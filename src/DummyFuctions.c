#include "DummyFunctions.h"
#include <math.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/time.h>


static double mean_delay = 0.0;
static long   count      = 0;
pthread_mutex_t mutex;


void *genericCos(void *args) {
    double angle = *(double*)args;
    double *ret  = malloc(sizeof(double));
    *ret = cos(angle);
    return ret;
}
void *genericSin(void *args) {
    double angle = *(double*)args;
    double *ret  = malloc(sizeof(double));
    *ret = sin(angle);
    return ret;
}
void *genericTimed(void *args) {
    double begin = *(double*)args;
    struct timeval t_end;
    gettimeofday(&t_end, NULL);
    double end = t_end.tv_sec*1000000.0 + t_end.tv_usec;
    double delay = end - begin;
    pthread_mutex_lock(&mutex);
    mean_delay = (count*mean_delay + delay)/(count+1);
    count++;
    pthread_mutex_unlock(&mutex);
    return (NULL);
}
void *genericPrint(void *args) {
    printf("Timed function ran %d. Current mean delay is %lf usec\n", count, mean_delay);
    return NULL;
}


void* (*dummyFunctions[4])(void*) = {genericCos, genericSin, genericTimed, genericPrint};



void dummy_init() {
    pthread_mutex_init(&mutex, NULL);
}